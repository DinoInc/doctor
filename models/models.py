# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Doctor(models.Model):
  _name = 'doctor.doctor'

  name = fields.Char(string="Name", required=True)

  address = fields.Text(string="Address")
  city = fields.Char(string="City")
  postalCode = fields.Char(string="Postal Code")
  state = fields.Char(string="Province")
  country = fields.Char(default="ID")