# -*- coding: utf-8 -*-
{
    'name': "Doctor",

    'summary': """
        Modul untuk menyimpan identitas dokter""",

    'description': """
        Digunakan untuk menambahkan dokter, mengedit data dokter dan menghapus dokter
    """,

    'author': "eHealth",
    'website': "http://www.ehealth.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'E-Health',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/doctor_security.xml',
        'views/doctor.xml',
        'security/ir.model.access.csv'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}